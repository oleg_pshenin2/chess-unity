using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace TestChess.View
{
    public class SquareBackgroundView : MonoBehaviour
    {
        [SerializeField] private Image _spriteRenderer;
        [SerializeField] private TextMeshProUGUI _coordsText;

        [SerializeField] private Color _whiteColor;
        [SerializeField] private Color _blackColor;

        [SerializeField] private bool _showDebugInfo = false;

        public void SetBackgroundWhite()
        {
            _spriteRenderer.color = _whiteColor;
        }

        public void SetBackgroundBlack()
        {
            _spriteRenderer.color = _blackColor;
        }

        public void SetupCoord(int index, int x, int y)
        {
            _coordsText.text = $"{index} : {x}, {y}";
            _coordsText.enabled = _showDebugInfo;
        }
    }
}