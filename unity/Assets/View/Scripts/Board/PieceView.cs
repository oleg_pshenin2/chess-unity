using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace TestChess.View
{
    public class PieceView : MonoBehaviour
    {
        [SerializeField] private Animator _animator;
        [SerializeField] private Image _icon;
        [SerializeField] private float _movingAnimationlength = 0.5f;

        public void SetupIcon(Sprite iconSprite)
        {
            _icon.sprite = iconSprite;
        }

        public void MoveTo(Vector3 position)
        {
            _animator.SetTrigger("Move");
            this.transform.DOMove(position, _movingAnimationlength, false);
        }

        public void Capture()
        {
            _animator.SetTrigger("Captured");
        }

        private void CapturedAnimationCompleted()
        {
            Destroy();
        }

        public void Destroy()
        {
            Destroy(this.gameObject);
        }
    }
}