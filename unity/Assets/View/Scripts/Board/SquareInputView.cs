using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace TestChess.View
{
    public class SquareInputView : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler, ISquareInputView
    {
        public event Action<int> PointerEnter = delegate { };
        public event Action<int> PointerExit = delegate { };
        public event Action<int> PointerUp = delegate { };
        public event Action<int> PointerDown = delegate { };
        public event Action<ISquareInputView> Destroyed = delegate { };

        private int _index;

        public void Init(int index)
        {
            _index = index;
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            PointerEnter(_index);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            PointerExit(_index);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            PointerUp(_index);
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            PointerDown(_index);
        }

        private void OnDestroy()
        {
            Destroyed(this);
        }
    }
}