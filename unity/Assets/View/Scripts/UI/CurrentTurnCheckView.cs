using UnityEngine;

namespace TestChess.View
{
    public class CurrentTurnCheckView : MonoBehaviour
    {
        [SerializeField] private GameObject _checkHolder;

        public void EnableCheckStatus()
        {
            _checkHolder.SetActive(true);
        }

        public void DisableCheckStatus()
        {
            _checkHolder.SetActive(false);
        }
    }
}