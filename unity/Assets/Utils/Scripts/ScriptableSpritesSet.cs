using UnityEngine;

namespace PB
{
    [CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/Scriptable Sprites Set", order = 2)]
    public class ScriptableSpritesSet : ScriptableGenericSet<string, Sprite, String_Sprite_Pair>
    {
        public Sprite GetSpriteByUid(string uid)
        {
            return GetValueByUid(uid);
        }

        public void Add(string uid, Sprite sprite)
        {
            _pairs.Add(new String_Sprite_Pair()
            {
                Key = uid,
                Value = sprite,
            });
        }
    }
}