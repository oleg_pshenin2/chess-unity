﻿using System.Collections.Generic;
using UnityEngine;

public static class ListExtensions
{
    public static void AddIfNotContains<T>(this List<T> list, T item)
    {
        if (!list.Contains(item))
            list.Add(item);
    }
}

public static class TransformExtensions
{
    public static Transform DestroyAllChildren(this Transform transform)
    {
        foreach (Transform child in transform)
        {
            GameObject.Destroy(child.gameObject);
        }

        return transform;
    }
}