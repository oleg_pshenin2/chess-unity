using System.Collections.Generic;
using TestChess.Model;
using TestChess.View;
using UnityEngine;

namespace TestChess.Presenter
{
    public class SquaresPresenter : MonoBehaviour
    {
        private IBoardLayoutProvider _boardLayoutProvider;
        private SquaresInputPresenter _squaresInputHandler;
        private ChessGame _game;
        private MovesHandler _movesRegistrator;
        private Dictionary<int, SquareView> _squares = new Dictionary<int, SquareView>();

        private List<int> _setAsAvailableToMove;
        private List<int> _setAsAvailableToCapture;
        private int _currentlyHovered;
        private int _currentlySelected;

        [SerializeField] private GameObject _squareViewPrefab;
        [SerializeField] private Transform _root;

        public void Init(IBoardLayoutProvider boardLayoutProvider, ChessGame game, SquaresInputPresenter squaresInputHandler, MovesHandler movesRegistrator)
        {
            Unsubscribe();

            _boardLayoutProvider = boardLayoutProvider;
            _squaresInputHandler = squaresInputHandler;
            _game = game;
            _movesRegistrator = movesRegistrator;

            GenerateSquares();

            Subscribe();
        }

        private void GenerateSquares()
        {
            _root.DestroyAllChildren();
            _squares.Clear();
            BoardUtils.IterateThroughBoard(GenerateSquare);
        }

        private void GenerateSquare(int x, int y)
        {
            var go = Instantiate(_squareViewPrefab, _root);
            var index = BoardUtils.GetIndexByCoords(x, y);
            go.transform.position = _boardLayoutProvider.GetLayoutByIndex(index).position;
            go.name = $"{index}: {x} - {y}";

            var squareView = go.GetComponent<SquareView>();
            _squares.Add(index, squareView);
        }

        private void Subscribe()
        {
            _squaresInputHandler.SquareHovered += SquareHoveredHandler;
            _squaresInputHandler.SquareUnhovered += SquareUnhoveredHandler;

            _movesRegistrator.PieceSelected += PieceSelectedHandler;
            _movesRegistrator.PieceDeselected += PieceDeselectedHandler;
        }

        private void Unsubscribe()
        {
            if (_squaresInputHandler != null)
            {
                _squaresInputHandler.SquareHovered -= SquareHoveredHandler;
                _squaresInputHandler.SquareUnhovered -= SquareUnhoveredHandler;
            }

            if (_movesRegistrator != null)
            {
                _movesRegistrator.PieceSelected -= PieceSelectedHandler;
                _movesRegistrator.PieceDeselected -= PieceDeselectedHandler;
            }
        }

        private void PieceSelectedHandler(int index)
        {
            DeselectCurrentSquare();
            SelectSquare(index);

            ResetPreviouslyAvailableToMoveOrCapture();
            SetSquaresAsAvailableToMoveOrCapture(index);
        }

        private void PieceDeselectedHandler()
        {
            DeselectCurrentSquare();
            ResetPreviouslyAvailableToMoveOrCapture();
        }

        private void SelectSquare(int index)
        {
            var squareView = GetSquareViewByIndex(index);
            squareView.SetAsSelected(true);

            _currentlySelected = index;
        }

        private void DeselectCurrentSquare()
        {
            if (_currentlySelected >= 0)
            {
                var squareView = GetSquareViewByIndex(_currentlySelected);
                squareView.SetAsSelected(false);
            }

            _currentlySelected = -1;
        }

        private void SquareHoveredHandler(int index)
        {
            if (_currentlyHovered == index)
                return;

            UnhoverCurrentSquare();
            HoverSquare(index);
        }

        private void SquareUnhoveredHandler(int index)
        {
            if (_currentlyHovered != index)
                return;

            UnhoverCurrentSquare();
        }

        private void HoverSquare(int index)
        {
            var squareView = GetSquareViewByIndex(index);
            squareView.SetAsHovered(true);

            _currentlyHovered = index;
        }

        private void UnhoverCurrentSquare()
        {
            if (_currentlyHovered >= 0)
            {
                var squareView = GetSquareViewByIndex(_currentlyHovered);
                squareView.SetAsHovered(false);
            }

            _currentlyHovered = -1;
        }


        private SquareView GetSquareViewByIndex(int index)
        {
            if (_squares == null)
            {
                Debug.LogError($"Square views weren't initialized");
                return null;
            }

            if (!_squares.ContainsKey(index))
            {
                Debug.LogError($"There is not square by index {index}");
                return null;
            }

            return _squares[index];
        }

        private void SetSquaresAsAvailableToMoveOrCapture(int index)
        {
            List<int> availableMoves;
            List<int> captureMoves;

            _game.AvailableMoves.GetAvailableMovesSplittedByCapturing(index, out availableMoves, out captureMoves);

            _setAsAvailableToMove = availableMoves;
            _setAsAvailableToCapture = captureMoves;

            foreach (var availableMove in availableMoves)
            {
                if (_squares.ContainsKey(availableMove))
                {
                    var squareView = _squares[availableMove];
                    squareView.SetAsAvailableToMove(true);
                }
            }

            foreach (var availableToCapture in captureMoves)
            {
                if (_squares.ContainsKey(availableToCapture))
                {
                    var squareView = _squares[availableToCapture];
                    squareView.SetAsAvailableToCapture(true);
                }
            }
        }

        private void ResetPreviouslyAvailableToMoveOrCapture()
        {
            if (_setAsAvailableToMove != null)
            {
                foreach (var availableMove in _setAsAvailableToMove)
                {
                    if (_squares.ContainsKey(availableMove))
                    {
                        var squareView = _squares[availableMove];
                        squareView.SetAsAvailableToMove(false);
                    }
                }

                _setAsAvailableToMove.Clear();
            }


            if (_setAsAvailableToCapture != null)
            {
                foreach (var availableToCapture in _setAsAvailableToCapture)
                {
                    if (_squares.ContainsKey(availableToCapture))
                    {
                        var squareView = _squares[availableToCapture];
                        squareView.SetAsAvailableToCapture(false);
                    }
                }

                _setAsAvailableToCapture.Clear();
            }
        }

        private void OnDestroy()
        {
            Unsubscribe();
        }
    }
}