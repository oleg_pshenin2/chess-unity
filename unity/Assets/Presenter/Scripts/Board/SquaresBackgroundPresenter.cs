﻿using TestChess.Model;
using TestChess.View;
using UnityEngine;

namespace TestChess.Presenter
{
    /// <summary>
    /// Generates board background, can be replaced with static bg image
    /// </summary>
    public class SquaresBackgroundPresenter : MonoBehaviour
    {
        [SerializeField] private GameObject _squareBGPrefab;
        [SerializeField] private Transform _root;

        private IBoardLayoutProvider _boardLayoutProvider;

        public void Init(IBoardLayoutProvider boardLayoutProvider)
        {
            _boardLayoutProvider = boardLayoutProvider;
            GenerateSquares();
        }

        private void GenerateSquares()
        {
            foreach (Transform child in _root)
            {
                GameObject.Destroy(child.gameObject);
            }

            BoardUtils.IterateThroughBoard(GenerateSquare);
        }

        private void GenerateSquare(int x, int y)
        {
            var go = Instantiate(_squareBGPrefab, _root);
            var index = BoardUtils.GetIndexByCoords(x, y);
            var color = BoardUtils.GetColorByCoords(x, y);
            var view = go.GetComponent<SquareBackgroundView>();

            go.transform.position = _boardLayoutProvider.GetLayoutByIndex(index).position;

            if (color == ChessColor.White)
                view.SetBackgroundWhite();
            else
                view.SetBackgroundBlack();

            view.SetupCoord(index, x, y);
        }
    }
}