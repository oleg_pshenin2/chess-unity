﻿using TestChess.Model;
using UnityEngine;

namespace TestChess.Presenter
{
    public class AppController : MonoBehaviour
    {
        [SerializeField] private BoardLayoutPresenter _boardLayoutProvider;
        [SerializeField] private SquaresBackgroundPresenter _squaresGenerator;
        [SerializeField] private SquaresInputPresenter _squaresInputHandler;
        [SerializeField] private PiecesPresenter _piecesPresenter;
        [SerializeField] private SquaresPresenter _squaresPresenter;
        [SerializeField] private CurrentTurnInfoPresenter _currentTurnInfoPresenter;
        [SerializeField] private GameResultPresenter _gameResultPresenter;
        [SerializeField] private GameControlButtonsPresenter _gameControlButtonsPresenter;

        private MovesHandler _movesRegistrator;

        private ChessGame _game;

        private void Start()
        {
            InitBoard();
            StartGame();
        }

        private void InitBoard()
        {
            _squaresGenerator.Init(_boardLayoutProvider);
        }

        private void StartGame()
        {
            _game = new ChessGame();

            _squaresInputHandler.Init(_boardLayoutProvider, _game);
            _piecesPresenter.Init(_boardLayoutProvider, _game);
            _movesRegistrator = new MovesHandler(_game, _squaresInputHandler);
            _squaresPresenter.Init(_boardLayoutProvider, _game, _squaresInputHandler, _movesRegistrator);

            _currentTurnInfoPresenter.Init(_game);
            _gameResultPresenter.Init(this, _game);

            _gameControlButtonsPresenter.Init(_game);
        }

        public void RestartGame()
        {
            StartGame();
        }
    }
}