using TestChess.Model;
using UnityEngine;
using UnityEngine.UI;

namespace TestChess.Presenter
{
    public class GameControlButtonsPresenter : MonoBehaviour
    {
        [SerializeField] private Button _drawButton;
        [SerializeField] private Button _giveUpButton;
        private ChessGame _game;

        private void Awake()
        {
            _drawButton.onClick.RemoveAllListeners();
            _drawButton.onClick.AddListener(OnDrawButtonClicked);

            _giveUpButton.onClick.RemoveAllListeners();
            _giveUpButton.onClick.AddListener(OnGiveUpButtonClicked);
        }

        public void Init(ChessGame game)
        {
            _game = game;
        }

        private void OnGiveUpButtonClicked()
        {
            _game?.GiveUpCurrent();
        }

        private void OnDrawButtonClicked()
        {
            _game?.Draw();
        }
    }
}