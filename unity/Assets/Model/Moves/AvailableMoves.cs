using System.Collections.Generic;
using System.Linq;

namespace TestChess.Model
{
    public class AvailableMoves : IAvailableMoves
    {
        private Dictionary<int, List<Move>> _cachedValidatedMoves = new Dictionary<int, List<Move>>();
        private Dictionary<int, List<Move>> _cachedNonValidatedMoves = new Dictionary<int, List<Move>>();

        private IBoard _board;
        private Square[] _squares;
        private PieceMoveSetsIterator _pieceMovementsFinder;
        private ChessColor _currentTurnPlayer;

        private bool _emulationInProcess;

        public AvailableMoves(IBoard board)
        {
            _board = board;
            _squares = _board.GetSquares();
            _pieceMovementsFinder = new PieceMoveSetsIterator(board);
        }

        public void SetCurrentPlayer(ChessColor player)
        {
            _currentTurnPlayer = player;
            CacheMoves(false);
        }

        public void GetAvailableMovesSplittedByCapturing(int fromIndex, out List<int> availableMoves, out List<int> captureMoves)
        {
            var validatedMoves = GetValidatedMovesForPieceFrom(fromIndex);
            var validatedMovesIndexes = validatedMoves.Select(x => x.MoveToIndex).ToList();

            var captureMovesTemp = validatedMovesIndexes.Where(x => _squares[x].Piece != null).ToList();

            availableMoves = validatedMovesIndexes.Where(x => !captureMovesTemp.Contains(x)).ToList();
            captureMoves = captureMovesTemp;
        }

        public bool HasPlayerMoves(ChessColor player)
        {
            var listOfSquaresWithPiecesOfPlayer = _board.GetPiecesIndexesByPlayer(player);

            foreach (var square in listOfSquaresWithPiecesOfPlayer)
            {
                var listOfMoves = GetValidatedMovesForPieceFrom(square.Index);

                if (listOfMoves.Count > 0)
                    return true;
            }

            return false;
        }

        public bool CanPieceMoveFromTo(int fromIndex, int toIndex)
        {
            return GetValidatedMovesForPieceFrom(fromIndex).ContainsMove(toIndex);
        }

        public void MakeAMove(int fromIndex, int toIndex)
        {
            var validatedMoves = GetValidatedMovesForPieceFrom(fromIndex);
            var move = validatedMoves.Find(x => x.MoveToIndex == toIndex);

            switch (move.GetMoveType())
            {
                case MoveType.Regular:
                    _board.MovePiece(fromIndex, toIndex);
                    break;

                case MoveType.Castling:
                    _board.MovePiece(fromIndex, toIndex);
                    _board.MovePiece(move.SpecialIndexes[0], move.SpecialIndexes[1]);
                    break;

                default:
                    break;
            }
        }

        public bool IsPlayerKingUnderAttack(ChessColor player)
        {
            var kingIndex = GetPlayerKingIndex(player);
            return CanPlayerMakeAMoveTo(player.GetOppositeColor(), kingIndex);
        }

        private void CacheMoves(bool clearOnly = true)
        {
            _cachedValidatedMoves.Clear();
            _cachedNonValidatedMoves.Clear();

            if (clearOnly)
                return;

            foreach (var square in _squares)
            {
                if (square.Piece == null || square.Piece.Color != _currentTurnPlayer)
                    continue;

                var index = square.Index;
                GetValidatedMovesForPieceFrom(index);
            }
        }

        private List<Move> GetValidatedMovesForPieceFrom(int fromIndex)
        {
            if (!_cachedValidatedMoves.ContainsKey(fromIndex))
            {
                var availableMoves = GetNonValidatedMovesForPieceFrom(fromIndex);
                availableMoves = ValidateMovesForCheckStatus(fromIndex, availableMoves);

                _cachedValidatedMoves.Add(fromIndex, availableMoves);
            }

            return _cachedValidatedMoves[fromIndex];
        }

        private List<Move> GetNonValidatedMovesForPieceFrom(int fromIndex)
        {
            if (!_cachedNonValidatedMoves.ContainsKey(fromIndex))
            {
                var availableMoves = GetRecalculatedNonValidatedMovesForPieceFrom(fromIndex);
                _cachedNonValidatedMoves.Add(fromIndex, availableMoves);
            }

            return _cachedNonValidatedMoves[fromIndex];
        }

        private List<Move> ValidateMovesForCheckStatus(int fromIndex, List<Move> moves)
        {
            var playerColor = _squares[fromIndex].Piece.Color;
            var validatedMoves = new List<Move>();

            _emulationInProcess = true;

            foreach (var move in moves)
            {
                if (move.GetMoveType() == MoveType.Castling)
                {
                    var castlingPieceUnderAttack = CanPlayerMakeAMoveTo(_currentTurnPlayer.GetOppositeColor(), fromIndex);
                    var castlingRookToIndexUnderAttack = CanPlayerMakeAMoveTo(_currentTurnPlayer.GetOppositeColor(), move.SpecialIndexes[1]);

                    if (castlingPieceUnderAttack || castlingRookToIndexUnderAttack)
                        continue;
                }

                var emulatedMove = new EmulatedBoardMove(_squares);
                emulatedMove.Move(fromIndex, move.MoveToIndex);

                if (!IsPlayerKingUnderAttack(playerColor))
                    validatedMoves.Add(move);

                emulatedMove.Rollback();
            }

            _emulationInProcess = false;

            return validatedMoves;
        }

        private List<Move> GetRecalculatedNonValidatedMovesForPieceFrom(int fromIndex)
        {
            return _pieceMovementsFinder.GetAvailableMovesForPieceFrom(fromIndex);
        }

        private int GetPlayerKingIndex(ChessColor player)
        {
            var listOfSquaresWithPiecesOfPlayer = _board.GetPiecesIndexesByPlayer(player);

            foreach (var square in listOfSquaresWithPiecesOfPlayer)
            {
                var piece = square.Piece;

                if (piece.GetPieceType() == PieceType.King)
                    return square.Index;
            }

            return -1;
        }

        private bool CanPlayerMakeAMoveTo(ChessColor player, int toIndex)
        {
            foreach (var square in _squares)
            {
                var piece = square.Piece;
                if (piece != null && piece.Color == player)
                {
                    if (_currentTurnPlayer == player)
                    {
                        if (GetValidatedMovesForPieceFrom(square.Index).ContainsMove(toIndex))
                            return true;
                    }
                    else
                    {
                        if (_emulationInProcess)
                        {
                            if (GetRecalculatedNonValidatedMovesForPieceFrom(square.Index).ContainsMove(toIndex))
                                return true;
                        }
                        else
                        {
                            if (GetNonValidatedMovesForPieceFrom(square.Index).ContainsMove(toIndex))
                                return true;
                        }
                    }
                }
            }

            return false;
        }
    }
}