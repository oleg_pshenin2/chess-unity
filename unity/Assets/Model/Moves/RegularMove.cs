namespace TestChess.Model
{
    public sealed class RegularMove : Move
    {
        public RegularMove(int moveToIndex) : base(moveToIndex) { }

        public override MoveType GetMoveType()
        {
            return MoveType.Regular;
        }
    }
}