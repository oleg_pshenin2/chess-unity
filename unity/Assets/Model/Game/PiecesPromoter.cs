namespace TestChess.Model
{
    public class PiecesPromoter
    {
        private const PieceType PROMOTE_TO_PIECE_TYPE = PieceType.Queen;
        private readonly IBoard _board;

        public PiecesPromoter(IBoard board)
        {
            _board = board;
        }

        public bool TryToPromotePieceAt(int index)
        {
            var square = _board.GetSquare(index);
            var coords = BoardUtils.GetCoordsByIndex(index);

            var piece = square.Piece;

            if (piece != null)
            {
                if (piece.CanBePromoted ())
                {
                    if (IsPromotionConditionSatisfied(piece, coords))
                    {
                        _board.SetNewPieceAt(index, PROMOTE_TO_PIECE_TYPE, piece.Color);
                        return true;
                    }
                }
            }

            return false;
        }

        private bool IsPromotionConditionSatisfied(Piece piece, Coords coords)
        {
            return (piece.Color == ChessColor.Black && coords.Y == 0)
                        || (piece.Color == ChessColor.White && coords.Y == Board.Size - 1);
        }
    }
}