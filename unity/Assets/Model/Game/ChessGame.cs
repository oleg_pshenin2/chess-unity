using System;

namespace TestChess.Model
{
    public class ChessGame
    {
        public event Action<ChessGameEnd, ChessColor> GameFinished = delegate { };
        public event Action TurnFinished = delegate { };
        public event Action<int> PiecePromotedAt = delegate { };

        public IBoard Board { get; private set; }
        public IAvailableMoves AvailableMoves { get; private set; }
        private PiecesPromoter _piecesPromoter;

        public ChessColor CurrentTurnSide { get; private set; }
        public int CurrentTurnNumber { get; private set; }
        public bool IsGameFinished { get; private set; }
        public bool IsCheckState { get; private set; }

        public ChessGame()
        {
            Board = new Board();
            Board.SetupDefaultLayout();

            AvailableMoves = new AvailableMoves(Board);
            _piecesPromoter = new PiecesPromoter(Board);

            CurrentTurnSide = ChessColor.White;
            CurrentTurnNumber = 1;

            IsCheckState = false;
            IsGameFinished = false;
            AvailableMoves.SetCurrentPlayer(CurrentTurnSide);
        }

        public void MakeAMove(int fromIndex, int toIndex)
        {
            if (IsGameFinished)
                return;

            if (AvailableMoves.CanPieceMoveFromTo(fromIndex, toIndex))
            {
                AvailableMoves.MakeAMove(fromIndex, toIndex);

                if (_piecesPromoter.TryToPromotePieceAt(toIndex))
                    PiecePromotedAt(toIndex);

                NextTurn();
            }
        }

        private void NextTurn()
        {
            IsCheckState = false;
            CurrentTurnSide = CurrentTurnSide.GetOppositeColor();
            AvailableMoves.SetCurrentPlayer(CurrentTurnSide);

            if (AvailableMoves.HasPlayerMoves(CurrentTurnSide))
            {
                if (AvailableMoves.IsPlayerKingUnderAttack(CurrentTurnSide))
                    IsCheckState = true;

                CurrentTurnNumber++;
                TurnFinished();
            }
            else
            {
                AvailableMoves.SetCurrentPlayer(CurrentTurnSide);

                if (AvailableMoves.IsPlayerKingUnderAttack(CurrentTurnSide))
                {
                    FinishGameWithResult(ChessGameEnd.Win, CurrentTurnSide.GetOppositeColor());
                }
                else
                {
                    FinishGameWithResult(ChessGameEnd.Pat, CurrentTurnSide.GetOppositeColor());
                }
            }
        }

        public void GiveUpCurrent()
        {
            FinishGameWithResult(ChessGameEnd.Win, CurrentTurnSide.GetOppositeColor());
        }

        public void Draw()
        {
            FinishGameWithResult(ChessGameEnd.Draw, CurrentTurnSide);
        }

        private void FinishGameWithResult(ChessGameEnd chessGameEnd, ChessColor side)
        {
            IsGameFinished = true;
            GameFinished(chessGameEnd, side);
        }
    }
}