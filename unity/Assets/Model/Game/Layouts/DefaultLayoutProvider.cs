using System.Collections.Generic;

namespace TestChess.Model
{
    public static class DefaultLayoutProvider
    {
        public static readonly Dictionary<int, PieceType> WhiteDefaultLayout = new Dictionary<int, PieceType>()
        {
            {0, PieceType.Rook},
            {1, PieceType.Knight},
            {2, PieceType.Bishop},
            {3, PieceType.King},
            {4, PieceType.Queen},
            {5, PieceType.Bishop},
            {6, PieceType.Knight},
            {7, PieceType.Rook},

            {8, PieceType.Pawn},
            {9, PieceType.Pawn},
            {10, PieceType.Pawn},
            {11, PieceType.Pawn},
            {12, PieceType.Pawn},
            {13, PieceType.Pawn},
            {14, PieceType.Pawn},
            {15, PieceType.Pawn},
        };

        public static readonly Dictionary<int, PieceType> BlackDefaultLayout = new Dictionary<int, PieceType>()
        {
            {56, PieceType.Rook},
            {57, PieceType.Knight},
            {58, PieceType.Bishop},
            {59, PieceType.King},
            {60, PieceType.Queen},
            {61, PieceType.Bishop},
            {62, PieceType.Knight},
            {63, PieceType.Rook},

            {48, PieceType.Pawn},
            {49, PieceType.Pawn},
            {50, PieceType.Pawn},
            {51, PieceType.Pawn},
            {52, PieceType.Pawn},
            {53, PieceType.Pawn},
            {54, PieceType.Pawn},
            {55, PieceType.Pawn},
        };
    }
}