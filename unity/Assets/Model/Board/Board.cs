using System;
using System.Collections.Generic;

namespace TestChess.Model
{
    public class Board : IBoard
    {
        public event Action<int, int> PieceMovedFromTo = delegate { };
        public event Action<int> PieceCreatedAt = delegate { };

        public const int Size = 8;
        private Square[] _squares;

        public Board()
        {
            InitSquares();
        }

        public void SetupLayout(Dictionary<int, PieceType> whiteLayout, Dictionary<int, PieceType> blackLayout)
        {
            InitPieces(whiteLayout, blackLayout);
        }

        public void SetupDefaultLayout()
        {
            InitPieces(DefaultLayoutProvider.WhiteDefaultLayout, DefaultLayoutProvider.BlackDefaultLayout);
        }

        public Square[] GetSquares()
        {
            return _squares;
        }

        public Square GetSquare(int index)
        {
            return _squares[index];
        }

        public Piece GetPiece(int index)
        {
            return GetSquare(index).Piece;
        }

        public Piece GetPiece(Coords coords)
        {
            return GetSquare(BoardUtils.GetIndexByCoords(coords)).Piece;
        }

        public List<Square> GetPiecesIndexesByPlayer(ChessColor player)
        {
            var listOfSquaresWithPiecesOfPlayer = new List<Square>();

            foreach (var square in _squares)
            {
                var piece = square.Piece;
                if (piece != null && piece.Color == player)
                {
                    listOfSquaresWithPiecesOfPlayer.Add(square);
                }
            }

            return listOfSquaresWithPiecesOfPlayer;
        }

        public void MovePiece(int fromIndex, int toIndex)
        {
            var fromSquare = GetSquare(fromIndex);
            var toSquare = GetSquare(toIndex);

            toSquare.Piece = fromSquare.Piece;
            fromSquare.Piece = null;

            toSquare.Piece.NumberOfMoves++;

            PieceMovedFromTo(fromIndex, toIndex);
        }

        public void SetNewPieceAt(int index, PieceType pieceType, ChessColor color)
        {
            var piece = PieceFactory.CreatePiece(pieceType, color);
            _squares[index].Piece = piece;

            PieceCreatedAt(index);
        }

        private void InitSquares()
        {
            _squares = new Square[Size * Size];

            for (int y = 0; y < Size; y++)
            {
                for (int x = 0; x < Size; x++)
                {
                    var index = BoardUtils.GetIndexByCoords(x, y);
                    _squares[index] = new Square(index);
                }
            }
        }

        private void InitPieces(Dictionary<int, PieceType> whiteLayout, Dictionary<int, PieceType> blackLayout)
        {
            foreach (var indexPiecePair in whiteLayout)
            {
                SetNewPieceAt(indexPiecePair.Key, indexPiecePair.Value, ChessColor.White);
            }

            foreach (var indexPiecePair in blackLayout)
            {
                SetNewPieceAt(indexPiecePair.Key, indexPiecePair.Value, ChessColor.Black);
            }
        }
    }
}