using System.Collections;
using System.Collections.Generic;

namespace TestChess.Model
{
    public abstract class PieceMovement : IEnumerable<Coords>
    {
        public abstract void SetupPieceCoords(Coords pieceCoords);

        public abstract IEnumerator<Coords> GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}