namespace TestChess.Model
{
    public sealed class PieceBishop : Piece
    {
        private readonly PieceMovement[] _pieceMovementSet = new PieceMovement[4]
        {
            new PieceMovementDirectional (PieceMovementDirections.UpRight),
            new PieceMovementDirectional (PieceMovementDirections.DownRight),
            new PieceMovementDirectional (PieceMovementDirections.DownLeft),
            new PieceMovementDirectional (PieceMovementDirections.UpLeft),
        };

        public PieceBishop(ChessColor color) : base(color) { }

        public override PieceMovement[] GetPieceMovementSet()
        {
            return _pieceMovementSet;
        }

        public override PieceType GetPieceType()
        {
            return PieceType.Bishop;
        }
    }
}