namespace TestChess.Model
{
    public sealed class PieceKing : Piece
    {
        private readonly PieceMovement[] _pieceMovementSet = new PieceMovement[8]
        {
            new PieceMovementDirectionalLimited (PieceMovementDirections.Up, 1),
            new PieceMovementDirectionalLimited (PieceMovementDirections.UpRight, 1),
            new PieceMovementDirectionalLimited (PieceMovementDirections.Right, 1),
            new PieceMovementDirectionalLimited (PieceMovementDirections.DownRight, 1),
            new PieceMovementDirectionalLimited (PieceMovementDirections.Down, 1),
            new PieceMovementDirectionalLimited (PieceMovementDirections.DownLeft, 1),
            new PieceMovementDirectionalLimited (PieceMovementDirections.Left, 1),
            new PieceMovementDirectionalLimited (PieceMovementDirections.UpLeft, 1),
        };

        public PieceKing(ChessColor color) : base(color) { }

        public override PieceMovement[] GetPieceMovementSet()
        {
            return _pieceMovementSet;
        }

        public override PieceType GetPieceType()
        {
            return PieceType.King;
        }

        public override bool CanBeCastled()
        {
            return true;
        }
    }
}