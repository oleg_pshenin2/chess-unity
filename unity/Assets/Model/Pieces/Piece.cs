namespace TestChess.Model
{
    public abstract class Piece
    {
        public int NumberOfMoves { get; set; } = 0;
        public ChessColor Color { get => _color; }

        // Color is constant during the game so it is readonly
        private readonly ChessColor _color;

        public Piece(ChessColor color)
        {
            _color = color;
        }

        public abstract PieceType GetPieceType();
        public abstract PieceMovement[] GetPieceMovementSet();

        public bool IsMoved()
        {
            return NumberOfMoves > 0;
        }

        public virtual bool IsAttackSetTheSameAsMovement()
        {
            return true;
        }

        public virtual PieceMovement[] GetPieceAttackSet()
        {
            return null;
        }

        public virtual bool CanBePromoted()
        {
            return false;
        }

        public virtual bool CanBeCastled()
        {
            return false;
        }
    }
}