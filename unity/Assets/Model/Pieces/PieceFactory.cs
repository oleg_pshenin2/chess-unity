namespace TestChess.Model
{
    public static class PieceFactory
    {
        public static Piece CreatePiece(PieceType pieceType, ChessColor color)
        {
            Piece piece = null;

            switch (pieceType)
            {
                case PieceType.King:
                    piece = new PieceKing(color);
                    break;
                case PieceType.Queen:
                    piece = new PieceQueen(color);
                    break;
                case PieceType.Bishop:
                    piece = new PieceBishop(color);
                    break;
                case PieceType.Knight:
                    piece = new PieceKnight(color);
                    break;
                case PieceType.Rook:
                    piece = new PieceRook(color);
                    break;
                case PieceType.Pawn:
                    piece = new PiecePawn(color);
                    break;
                default:
                    break;
            }

            return piece;
        }
    }
}