namespace TestChess.Model
{
    public sealed class PieceKnight : Piece
    {
        private readonly PieceMovement[] _pieceMovementSet = new PieceMovement[8]
        {
            new PieceMovementOffsetJumpLimited (new Coords (1, 2), 1),
            new PieceMovementOffsetJumpLimited (new Coords (2, 1), 1),
            new PieceMovementOffsetJumpLimited (new Coords (2, -1), 1),
            new PieceMovementOffsetJumpLimited (new Coords (1, -2), 1),
            new PieceMovementOffsetJumpLimited (new Coords (-1, -2), 1),
            new PieceMovementOffsetJumpLimited (new Coords (-2, -1), 1),
            new PieceMovementOffsetJumpLimited (new Coords (-2, 1), 1),
            new PieceMovementOffsetJumpLimited (new Coords (-1, 2), 1),
        };

        public PieceKnight(ChessColor color) : base(color) { }

        public override PieceMovement[] GetPieceMovementSet()
        {
            return _pieceMovementSet;
        }

        public override PieceType GetPieceType()
        {
            return PieceType.Knight;
        }
    }
}